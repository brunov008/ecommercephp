<?php
 
namespace App\Repositories;

class UserRepository{ 

	public function __construct(){

	}
	
	public function find($user){ 
		//MOCK USERS
		$strJsonFileContents = file_get_contents(app_path()."/Http/Controllers/MockJson/login.json");

		$array = json_decode($strJsonFileContents, true);
		
		foreach ($array['users'] as $mockUser) {
			if($user->cpf == $mockUser['cpf'] && 
				$user->role == $mockUser['role'] && 
				$user->senha == $mockUser['senha']){
				return $mockUser;
			}
		}

		return null;
	}

	public function create($user){
		//MOCK USERS
		$strJsonFileContents = file_get_contents(app_path()."/Http/Controllers/MockJson/login.json");

		$array = json_decode($strJsonFileContents, true);

		array_push($array['users'], $user);

		$newJsonString = json_encode($array, JSON_PRETTY_PRINT);
		file_put_contents(app_path()."/Http/Controllers/MockJson/login.json", $newJsonString);

		return true;
	}
}