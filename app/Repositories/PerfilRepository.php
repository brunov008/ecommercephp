<?php
 
namespace App\Repositories;

class PerfilRepository{

	public function __construct(){

	}
	
	public function getRoupas(){
		//MOCK ROUPAS
		$strJsonFileContents = file_get_contents(app_path()."/Http/Controllers/MockJson/roupas.json");
		$array = json_decode($strJsonFileContents, true);

		return $array;
	}

	public function destroy($id) : bool{
		//MOCK ROUPAS
		$strJsonFileContents = file_get_contents(app_path()."/Http/Controllers/MockJson/roupas.json");
		$array = json_decode($strJsonFileContents, true);

		foreach ($array['roupas'] as $key => $value) {
			if($value['id'] == $id){
				array_splice($array['roupas'],$key,1);

				$newJsonString = json_encode($array, JSON_PRETTY_PRINT);
				file_put_contents(app_path()."/Http/Controllers/MockJson/roupas.json", $newJsonString);

				return true;
			}
		}
		return false;
	} 
}