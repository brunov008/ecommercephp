<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Repositories\UserRepository;
use App\User;

class LoginController extends Controller
{
    use AuthenticatesUsers;

     /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $users;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectToPerfil = 'perfil';

    /**
     * Where to redirect users after create an account.
     *
     * @var string
     */
    protected $redirectToHome = 'home';

    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->users = new UserRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('login');
    }

    public function cadastrarIndex()
    {
        return view('cadastrar');
    }

    public function doLogin(Request $request){

        $user = new User;
        $user->cpf = $request->input('user');
        $user->senha = $request->input('password');
        $user->role = $request->input('role');
        $user->remember_token = $request->session()->get('_token');

        if ($this->users->find($user) != null) {
            $validUser = $this->users->find($user);  //TODO query desnecessaria, realizar funcao de callback
            $user->nome = $validUser['nome'];
            $user->email = $validUser['email'];
            $user->celular = $validUser['celular'];
            $user->telefone = $validUser['telefone'];


            $request->session()->put('user', $user); 
            return redirect()->route($this->redirectToPerfil, ['id' => $request->session()->getId()]);    
        }else{
            return redirect('login')->with('status', 'Usuario não encontrado');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->input('password') != $request->input('passwordConfirm')){
            return redirect('cadastrar')->with('status', 'Senhas devem ser iguais');
        }

        $user = new User;
        $user->cpf = $request->input('user');
        $user->senha = $request->input('password');
        $user->role = "usuario";
        $user->nome = $request->input('nome');
        $user->email = $request->input('email');
        $user->celular = $request->input('celular');
        $user->telefone = $request->input('telefone');

        if ($this->users->create($user)) {
            $request->session()->put('user', $user);
            return redirect()->route($this->redirectToHome, ['id' => $request->session()->getId()]);    
        }else{
            return redirect('cadastrar')->with('status', 'Ocorreu um erro inesperado');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
