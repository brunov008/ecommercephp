<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CarrinhoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mockItens = array(
            'roupas' => array( 
                array(
                    'nome' => 'calca1',
                    'imagem' => null,
                    'detalhe' => "detalhe do produto 1",
                    'preco' => 300
                ),
                array(
                    'nome' => 'calca3',
                    'imagem' => null,
                    'detalhe' => "detalhe do produto 2",
                    'preco' => 320)
            ),
            'subtotal' => 0
        );

        foreach ($mockItens['roupas'] as $value) {
            $value['imagem'] = $value['nome'].".jpg";
            $mockItens['subtotal'] += $value['preco'];
        } 

        return view('carrinho', $mockItens);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
