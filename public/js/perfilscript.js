const URL = "http://localhost:8000"

$('#create-edit-Modal').on('show.bs.modal', (event) => {
  const button = $(event.relatedTarget) // Botão que acionou o modal
  const type = button.data('type') // Extrai informação dos atributos data-*
  const modal = $(event.currentTarget)

  switch(type){
    case "update":
      let roupa = new Roupa(
        button.data('id'), 
        button.data('name'), 
        button.data('imagem'), 
        button.data('preco'), 
        button.data('detalhe'))

      $(event.currentTarget).find('#image-modal').attr("src",roupa.imagem)
      $(event.currentTarget).find('#form-name').val(roupa.name)
      $(event.currentTarget).find('#form-preco').val(roupa.preco)
      $(event.currentTarget).find('#form-detalhe').text(roupa.detalhe)

      $(event.currentTarget).find('#button-edit-create-footer').click(()=>{
        $(event.currentTarget).modal('hide')
        alert("click")
      })
    break;

    case "create":
      console.log("Criar novo item")
    break

    default:
      alert("Ocorreu um erro inesperado")
    break
  }
})

$('#excluirModal').on('show.bs.modal', (event) => {
  const button = $(event.relatedTarget)
  
  let roupa = new Roupa(
        button.data('id'), 
        button.data('name'))

  $(event.currentTarget).find('.modal-title').text('Deseja realmente apagar o item ' + roupa.name + "?")

  $(event.currentTarget).find('#button-excluir-footer').click(()=>{
    $(event.currentTarget).modal('hide')
    excludeRoupa(roupa.id)
  })
})

function excludeRoupa(id){
  request(
    URL + '/perfil/roupas/' + id, 
    'DELETE',
    null
  )
}

function request(targetURL, type, data){
  $.ajax({
    url: targetURL,
    type: type,
    //contentType: "application/x-www-form-urlencoded; charset=UTF-8",
    data: data,
    cache: false,
    timeout: 3000
  })
  .done((data)=>{
      alert(data)
   })
  .fail(()=>{
      alert("Ocorreu um erro ao se conectar com o servidor, tente mais tarde!")
    });
}

class Roupa{
  constructor(id, name, imagemPath, preco, detalhe) {
    this.id = id
    this.name = name
    this.imagemPath = imagemPath
    this.preco = preco
    this.detalhe = detalhe
  }
}