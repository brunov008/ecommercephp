<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//HOME
Route::permanentRedirect('/', 'home');
Route::match(['get', 'post'], '/home/{id?}', 'HomeController@index')->name('home'); 

//LOGIN
Route::get('/login', 'LoginController@index');
Route::post('/login', 'LoginController@doLogin');
Route::get('/login/cadastrar', 'LoginController@cadastrarIndex')->name('cadastrar');
Route::post('/login/cadastrar/criar', 'LoginController@store');

//ENTREGA
Route::get('/entrega', 'EntregaController@index');

//CARRINHO
Route::get('/carrinho/{id?}', 'CarrinhoController@index')->name('carrinho');

//PERFIL
Route::get('/perfil', 'PerfilController@index')->name('perfil');
Route::put('/perfil/roupas/{idRoupa}', 'PerfilController@edit'); //update
Route::delete('/perfil/roupas/{idRoupa}', 'PerfilController@destroy');//delete
Route::post('/perfil/roupas', 'PerfilController@store'); //create 
