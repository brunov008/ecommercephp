<!DOCTYPE html>
<html lang="pt-br">
    <head>
    <title>Perfil</title>

    @include('partials.perfil.head')
    </head>
    <body>
        <header>
            @include('partials.perfil.nav')
        </header>
            <div class="d-flex bd-highlight mt-3"> 
                <div class="bd-highlight p-3">
                    <div class="d-flex align-items-start flex-column" style="height: 450px;">
                        @if (session('user')->role == 'admin')
                            <button id="btn-roupas-cadastradas" onclick="getRoupas()" class="container-fluid mb-5 p-2 bg-warning">Cadastrar Roupas</button>
                            <!--
                            <button id="" class="rounded container-fluid mb-5 p-2 bg-warning">Button 2</button>
                            <button id="" class="rounded container-fluid mb-5 p-2 bg-warning">Button 3</button>
                            -->
                        @endif

                        @if (session('user')->role == 'usuario')
                            <h3 class="mb-5">Olá {{session('user')->nome}} :)</h3>
                            <button class="rounded container-fluid mb-5 p-2 bg-warning">Meus ultimos pedidos</button>
                            <button class="rounded container-fluid mb-5 p-2 bg-warning">Meu cartão cadastrado</button>
                            <button class="rounded container-fluid mb-5 p-2 bg-warning">Meu endereço</button>
                            <button class="rounded container-fluid mt-auto mb-5 p-2 bg-warning">Sair</button>    
                        @endif
                    </div>
                </div>
                <div class="flex-grow-1 bd-highlight p-3"> 
                    <div id="main-container">
                        @if (session('user')->role == 'admin')
                            @include('partials.perfil.admin_list')
                        @endif

                        @if (session('user')->role == 'usuario')
                            @include('partials.perfil.user_list')
                        @endif
                    </div>
                </div>
            </div>

        <!-- FOOTER -->
        @if(session('user')->role == 'admin')
            <footer class="d-flex fixed-bottom justify-content-center p-3">
              <button class="custom-perfil-button" data-toggle="modal" data-target="#create-edit-Modal" data-type="create">
                    <image class="bg-dark custom-perfil-image rounded-circle p-2" src="{!! asset('images/ic_add.png') !!}"/>
              </button>
            </footer>
        @endif

        <script type="text/javascript" src="{!! asset('js/perfilscript.js') !!}"></script>
    </body>
</html>