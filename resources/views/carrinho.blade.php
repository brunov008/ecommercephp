<!DOCTYPE html>
<html lang="pt-br">
    <head>
    <title>Carrinho</title>

    @include('partials.carrinho.head')
    </head>
    <body>
        <header>
            @include('partials.carrinho.nav')
        </header>

        <div class="container-fluid mt-3">
            <div class="row">
                <div class="col-9 pl-5 pt-2">
                    <div class="d-flex flex-column">
                        <h2>Itens no Carrinho</h2>
                        <div class="d-flex justify-content-end">Preços</div>
                        <div class="custom-row mt-2"></div>

                        @foreach ($roupas as $roupa)
                            <div class="d-flex flex-row pl-4 pt-4 pb-4">
                                <!-- $roupa['imagem']  TODO ajustar a imagem -->
                                <image src="{!! asset('images/calca1.jpg') !!}" height="100" width="100"/>

                                <div class="d-flex flex-column ml-5">
                                    <h3>{{$roupa['nome']}}</h3>
                                    <h4>{{$roupa['detalhe']}}</h4>
                                    <div class="mt-2">
                                        <div class="btn-group" role="group">
                                            <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">qtd
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                              <a class="dropdown-item" href="#">1</a>
                                              <a class="dropdown-item" href="#">2</a>
                                              <a class="dropdown-item" href="#">3</a>
                                              <a class="dropdown-item" href="#">4</a>
                                              <a class="dropdown-item" href="#">5</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container-fluid p-0">
                                    <div class="d-flex justify-content-end text-danger">
                                        <h5>R${{$roupa['preco']}}</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="custom-row mt-2"></div>
                        @endforeach
                    </div>
                </div>
                <div class="col-3">
                    <div class="d-flex flex-column border border-dark shadow p-3">
                        <div class="d-flex justify-content-center mb-4">Subtotal R${{$subtotal}}</div>
                        <a class="d-flex justify-content-center" href="{!! url('entrega') !!}"><button class="btn btn-primary w-100">Prosseguir</button></a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>