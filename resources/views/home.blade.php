<!DOCTYPE html>
<html lang="pt-br" class="full-height">
  <head>
    <title>Home</title>

    @include('partials.home.head')
  </head>

  <body>
    <!-- HEADER --> 
    <header>
      @include('partials.home.nav')
    </header>

    <div class="view intro-2">
      <div class="full-bg-img"></div>
    </div>

    <!-- Categoria Container TODO-->
    <main class="pt-5">
      @include('partials.home.categoria')
    </main>

    <!-- FOOTER -->
    <footer class="custom-footer fixed-bottom container-fluid">
      <div class="container text-center">
          <a href="#">
            <image class="bg-white rounded-circle p-2" src="{!! asset('images/ic_message.png') !!}" height="60" width="60"/>
          </a>
      </div>
    </footer>

    <script type="text/javascript">
      $(document).ready(() => {
        $("#myInput").on("keyup",() => {
          var value = $(this).val().toLowerCase();
          $("#myList li").filter(() => {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });
      });
    </script>
  </body>
</html>