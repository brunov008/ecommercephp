<!DOCTYPE html>
<html lang="pt-br">
    <head>
    <title>Carrinho</title>

    @include('partials.login.cadastrar.head')
    </head>
    <body>
        <header>
            @include('partials.login.cadastrar.nav')
        </header>
        <div class="d-flex justify-content-center m-4 mt-5"> 
            <div class="p-5 rounded border border-secondary">
                <div class="text-center">
                    <h2 id="login-title">Cadastro</h2>
                </div>
                <form method="POST" action="{{ url('/login/cadastrar/criar') }}">
                  @csrf  
                  <div class="form-group">
                    <label for="nome">Nome</label>
                    <input type="text" class="form-control" id="nome" name="nome">
                  </div>
                  <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="text" class="form-control" id="email" name="email">
                  </div>
                  <div class="form-group">
                    <label for="user">Cpf/Cnpj</label>
                    <input type="text" class="form-control" id="user" name="user">
                  </div>
                  <div class="form-group">
                    <label for="celular">Celular</label>
                    <input type="text" class="form-control" id="celular" name="celular">
                  </div>
                  <div class="form-group">
                    <label for="telefone">Telefone</label>
                    <input type="text" class="form-control" id="telefone" name="telefone">
                  </div>
                  <div class="form-group">
                    <label for="password">Senha</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="******">
                  </div>
                  <div class="form-group">
                    <label for="passwordConfirm">Confirmar Senha</label>
                    <input type="password" class="form-control" id="passwordConfirm" name="passwordConfirm" placeholder="******">
                  </div>

                  <div class="d-flex justify-content-center"> 
                    <button type="submit" class="btn border">Cadastrar</button>
                  </div>
                </form>
            </div>
        </div>
    </body>
</html>