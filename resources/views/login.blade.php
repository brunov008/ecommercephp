<!DOCTYPE html>
<html lang="pt-br">
    <head>
    <title>Login</title>

    @include('partials.login.head')
    </head>
    <body>
        <header>
            @include('partials.login.nav')
        </header>

        @if (session('status'))
            <script>
              alert("{{session('status')}}");
            </script>
        @endif
        <div class="d-flex justify-content-center m-4 mt-5"> 
            <div class="p-5 rounded border border-secondary">
                <div class="text-center">
                    <h2 id="login-title">Login de Usuário</h2>
                </div>
                <form method="POST" action="{{ url('login') }}">
                  @csrf  
                  <div class="form-group">
                    <label for="user">Cpf/Cnpj</label>
                    <input type="text" class="form-control" id="user" name="user">
                  </div>
                  <div class="form-group">
                    <label for="password">Senha</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="******">
                  </div>

                  <div class="d-flex justify-content-between">
                    <div class="d-flex flex-row">
                      <input type="radio" id="radio-usuario" name="role" value="usuario" checked="checked">
                      <label for="radio-usuario">Usuario</label>
                    </div>
                    <div class="d-flex flex-row">
                      <input type="radio" id="radio-adm" name="role" value="admin">
                      <label for="radio-adm">Administrador</label>
                    </div>
                  </div>

                  <div class="d-flex justify-content-center"> 
                    <button type="submit" class="btn border">Login</button>
                  </div>
                </form>
                <div id="facebook-container" class="d-flex flex-column text-center">
                    <label class="m-3 mb-1">ou</label>
                    <button type="submit" class="btn text-white mb-2" style="background: #3b5998;">Entrar com Facebook</button>
                    <a href="{{ url('login/cadastrar') }}">Cadastrar Nova Conta</a>
                    <a href="#">Esqueci Minha Senha</a>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $('#radio-usuario').click(() => {
                $('#login-title').text("Login de Usuário"); 

                if ( document.getElementById("facebook-container").classList.contains('invisible') ){
                    document.getElementById("facebook-container").classList.remove("invisible");
                }
            });

            $('#radio-adm').click(() => {
                $('#login-title').text("Login de Administrador");
                document.getElementById("facebook-container").classList.add("invisible");
            });
        </script>
    </body>
</html>