<nav class="navbar navbar-expand-lg shadow bg-white rounded">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="{!! url('home') !!}">
          <image src="{!! asset('images/roupasenaclogo.png') !!}" height="60" width="60"/>
      </a>
    </div>
    <!--
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    -->
    <ul class="nav navbar-nav navbar-right">
      
      <li class="pr-4"><a class="text-decoration-none text-dark" href="{!! url('login') !!}"><span><image src="{!! asset('images/ic_person.png') !!}" height="40" width="40"/></span> Realizar Login</a></li>

      <li><a href="{!! url('carrinho') !!}"><span><image src="{!! asset('images/cesta.png') !!}" height="40" width="40"/></span></a></li>
    </ul>
  </div>
</nav>