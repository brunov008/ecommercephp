<div class="d-flex flex-column p-3">
	@foreach($roupas as $roupa)
		<div class="d-flex flex-row pl-4 pt-4 pb-4 shadow-sm rounded" style="background: #F8F8FF">
            <!-- $roupa['imagem']  TODO ajustar a imagem -->
            <image src="{!! asset('images/calca1.jpg') !!}" height="100" width="100"/>

            <div class="d-flex flex-column ml-5">
                <h3>{{$roupa['nome']}}</h3>
                <h4>{{$roupa['detalhe']}}</h4>
                
            </div>
            <div class="container-fluid p-0">
                <div class="d-flex justify-content-end">
                    <div class="d-flex flex-row">
                        <button class="custom-perfil-button mr-3" data-toggle="modal" data-target="#create-edit-Modal"  data-type="update" data-id="{{$roupa['id']}}" data-name="{{$roupa['nome']}}" data-imagem="{{$roupa['imagem']}}" 
                        data-preco="{{$roupa['preco']}}" data-detalhe="{{$roupa['detalhe']}}">
                    	   <image class="custom-perfil-image" src="{{ asset('images/ic_edit.png') }}"/>
                        </button>
                        <button class="custom-perfil-button" data-toggle="modal" data-target="#excluirModal" data-id="{{$roupa['id']}}" data-name="{{$roupa['nome']}}">
                    	   <image class="custom-perfil-image" src="{{ asset('images/ic_close.png') }}"/>
                        </button>
                    </div>
                </div>
            </div>
        </div> 
        <div class="custom-row mt-2"></div>
	@endforeach
</div>

<!-- MODAL criar e editar -->
<div class="modal fade" id="create-edit-Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document"> 
    <div class="modal-content">
      <div class="modal-body">
          <form>
            <div class="d-flex justify-content-center">
              <img id="image-modal" src="#" class="d-flex" style="height: 70px; width: 70px">
            </div>
            <div class="form-group"> 
              <label for="form-name" class="col-form-label">Nome:</label>
              <input type="text" class="form-control" id="form-name">
            </div>
            <div class="form-group">
             <label for="form-preco" class="col-form-label">Preço:</label>
             <input type="text" class="form-control" id="form-preco" placeholder="R$100,00">

             <label for="form-detalhe" class="col-form-label">Detalhes:</label>
             <textarea id="form-detalhe" class="form-control" rows="3"></textarea>
            </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal">Cancelar</button>
        <button id ="button-edit-create-footer" type="button" class="btn">Enviar</button>
      </div>
    </div>
  </div>
</div> 

<!-- Modal excluir -->
<div class="modal fade" id="excluirModal" tabindex="-1" role="dialog" aria-labelledby="TituloModalLongoExemplo" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="TituloModalLongoExemplo"></h5>
      </div>
      <div class="modal-footer">
        <button id="button-excluir-footer" type="button" class="btn btn-danger">Excluir</button>
        <button type="button" class="btn" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>