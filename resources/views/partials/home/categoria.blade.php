<div class="container-fluid">
	<div class="d-flex flex-column">
		<h2 class="d-flex justify-content-center">Categoria</h2>

		<div class="d-flex justify-content-between container-fluid bg-red">
			<h5>1-48 de 1000 resultados para categoria</h5>
			
			<div class="btn-group" role="group">
	            <button id="categoriaButton" type="button" class="btn btn-yellow dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Ordenar por
	            </button>
	            <div class="dropdown-menu" aria-labelledby="categoriaButton">
	              <a class="dropdown-item" href="#">preço</a>
	              <a class="dropdown-item" href="#">ordem alfabética</a>
	            </div>
	        </div>
		</div>

		<div class="d-flex flex-column"> 
	    	@for($i = 0; $i < 3; $i++)
		    	<div class="card-deck mt-4">
				  <div class="card">
				    <img src="..." class="card-img-top" alt="...">
				    <div class="card-body">
				      <h5 class="card-title">Card title</h5>
				      <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
				    </div>
				  </div>
				  <div class="card">
				    <img src="..." class="card-img-top" alt="...">
				    <div class="card-body">
				      <h5 class="card-title">Card title</h5>
				      <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
				    </div>
				  </div>
				  <div class="card">
				    <img src="..." class="card-img-top" alt="...">
				    <div class="card-body">
				      <h5 class="card-title">Card title</h5>
				      <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
				    </div>
				  </div>
				</div>
			@endfor
		</div>
	</div>
</div>