<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1">
    
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{!! asset('css/bootstrap/bootstrap.min.css') !!}" crossorigin="anonymous">

<!-- Custom CSS -->
<link rel="stylesheet" href="{!! asset('css/style.css') !!}" type="text/css"> 

<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" crossorigin="anonymous"></script>
<script src="{!! asset('js/bootstrap/bootstrap.min.js') !!}" crossorigin="anonymous"></script>